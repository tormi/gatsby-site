/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 * See: https://www.gatsbyjs.org/tutorial/part-seven/
 */

const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions
  if (node.internal.type === `ApiariesCsv`) {
    const slug = createFilePath({ node, getNode, basePath: `mesila` })
    createNodeField({
      node,
      name: `slug`,
      value: slug,
    })
  }
}

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions
  return graphql(`
    {
      allApiariesCsv {
        edges {
          node {
            asuk_registri_nr
          }
        }
      }
    }
  `).then(result => {
    result.data.allApiariesCsv.edges.forEach(({ node }) => {
      createPage({
        path: '/mesila/' + node.asuk_registri_nr,
        component: path.resolve(`./src/templates/mesila.js`),
        context: {
          // Data passed to context is available
          // in page queries as GraphQL variables.
          slug: node.asuk_registri_nr,
        },
      })
    })
  })
}
