import React from 'react'
import { Map, CircleMarker, Popup, LayersControl } from 'react-leaflet'
import { Link, graphql } from 'gatsby'
import NoSSR from 'react-no-ssr'
import WMTSTileLayer from 'react-leaflet-wmts'
import Layout from '../components/layout'
import SEO from '../components/seo'
import parse from 'wellknown'
import './index.css'
import MarkerClusterGroup from 'react-leaflet-markercluster'
import 'leaflet.markercluster'
require('react-leaflet-markercluster/dist/styles.min.css')

// const { BaseLayer, Overlay } = LayersControl

// See: https://medium.com/@kyleshevlin/how-to-use-client-side-libraries-in-a-universal-javascript-app-aa552b715f06
const isBrowser = typeof window !== 'undefined'
const { BaseLayer, Overlay } = isBrowser ? LayersControl : () => {}

const Loading = () => <div>Loading...</div>

export default ({ data }) => {
  if (typeof window !== 'undefined') {
    return (
      <Layout>
        <SEO title="Mesilate kaart" keywords={[`talu.app`, `farming`]} />
        <h3>
          Mesilate kaart{' '}
          <span style={{ fontSize: 14 }}>
            {' '}
            (kokku {data.allApiariesCsv.totalCount})
          </span>
        </h3>
        <NoSSR onSSR={<Loading />}>
          <Map
            key={Math.random()}
            className="leaflet-container"
            center={[58.58, 25.53]}
            zoom={7}
            maxZoom={18}
            // Add some speed: https://stackoverflow.com/a/54006851
            preferCanvas={true}
          >
            <LayersControl position="topright">
              <BaseLayer checked name="Kaart">
                <WMTSTileLayer
                  attribution="Kaart: Maa-amet | Andmed: PRIA"
                  url="https://tiles.maaamet.ee/tm/wmts"
                  layer="kaart"
                  tilematrixSet="GMC"
                  format="image/png"
                  transparent={true}
                  opacity={1}
                />
              </BaseLayer>
              <BaseLayer name="Foto">
                <WMTSTileLayer
                  attribution="Kaart: Maa-amet | Andmed: PRIA"
                  url="https://tiles.maaamet.ee/tm/wmts"
                  layer="foto"
                  tilematrixSet="GMC"
                  format="image/png"
                  transparent={true}
                  opacity={1}
                />
              </BaseLayer>
              <Overlay name="Hübriid">
                <WMTSTileLayer
                  attribution="Kaart: Maa-amet | Andmed: PRIA"
                  url="https://tiles.maaamet.ee/tm/wmts"
                  layer="hybriid"
                  tilematrixSet="GMC"
                  format="image/png"
                  transparent={true}
                  opacity={1}
                />
              </Overlay>
              <Overlay checked name="Mesilad">
                <NoSSR onSSR={<Loading />}>
                  <MarkerClusterGroup
                    disableClusteringAtZoom={13}
                    chunkedLoading={true}
                  >
                    {data.allApiariesCsv.edges.map(({ node }, id) => {
                      // Convert WKT points to JSON lng, lat array.
                      let geometry = parse(node.geometry)
                      let lat = geometry['coordinates'][0]
                      let lng = geometry['coordinates'][1]

                      let path = '/mesila/' + node.pohi_nr

                      // Join phone fields.
                      let phone = [node.reg_mobiil, node.reg_telefon]
                        .filter(function(item) {
                          return item != null
                        })
                        .join(' ')

                      let markersComponent
                      markersComponent = (
                        <CircleMarker
                          key={id}
                          center={[lng, lat]}
                          fillOpacity={0.7}
                          fillColor={`#ffcc00`}
                          color={`#FFFFFF`}
                          weight={1}
                          opacity={1}
                          radius={16}
                        >
                          <Popup>
                            <p>
                              <strong>
                                <Link to={path}>
                                  {node.pohi_nr} {node.aadr_aadress}
                                </Link>
                              </strong>
                              <br />
                              <strong>Mesinik: </strong>
                              {node.vastut_nimi}
                              <br />
                              <strong>Telefon: </strong>
                              {phone}
                              <br />
                              <strong>Liik: </strong>
                              {node.liik_tekst}
                              <br />
                              <strong>PRIA's alates: </strong>
                              {node.reg_kp}
                              <br />
                            </p>
                          </Popup>
                        </CircleMarker>
                      )
                      return markersComponent
                    })}
                  </MarkerClusterGroup>
                </NoSSR>
              </Overlay>
            </LayersControl>
          </Map>
        </NoSSR>
      </Layout>
    )
  }
  return null
}

export const query = graphql`
  query {
    allApiariesCsv(
      sort: { fields: [reg_kp], order: DESC }
      filter: { staatus_tekst: { eq: "Kehtiv" } }
    ) {
      totalCount
      edges {
        node {
          id
          pohi_nr
          aadr_aadress
          aadr_kyla_alev
          aadr_valdlinn_tekst
          aadr_maakond_tekst
          vastut_nimi
          reg_kp(formatString: "DD. MMMM YYYY", locale: "et-EE")
          reg_mobiil
          reg_telefon
          geometry
          staatus_tekst
          loomaliigid
          liik_tekst
        }
      }
    }
  }
`
