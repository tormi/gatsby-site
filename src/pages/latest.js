import React from 'react'
import { Link, graphql } from 'gatsby'
import Layout from '../components/layout'
import SEO from '../components/seo'
// import wellknown from 'wellknown'
import './latest.css'

// See: https://medium.com/@kyleshevlin/how-to-use-client-side-libraries-in-a-universal-javascript-app-aa552b715f06
// const isBrowser = typeof window !== 'undefined'
// const { wellknown } = isBrowser ? require('wellknown') : () => {}

export default ({ data }) => {
  return (
    <Layout>
      <SEO
        title="100 värsket mesilat"
        keywords={[`mesi.ee`, `mesi`, `mesindus`]}
      />
      <div>
        <h3>
          100 värsket mesilat{' '}
          <span style={{ fontSize: 14 }}>
            {' '}
            (kokku {data.allApiariesCsv.totalCount})
          </span>
        </h3>
        <table>
          <thead>
            <tr>
              <th>Mesila</th>
              <th>Mesinik</th>
              {/* <th>Telefon</th> */}
              <th>Maakond</th>
              <th>PRIA's alates</th>
            </tr>
          </thead>
          <tbody>
            {data.allApiariesCsv.edges.map(({ node }, id) => {
              // Convert WKT points to JSON lng, lat array.
              // let geometry = wellknown(node.geometry)
              // let coordinates = geometry['coordinates']
              // let reverse = coordinates.reverse()
              // let point = JSON.stringify(reverse)
              let path = '/mesila/' + node.asuk_registri_nr

              // Join phone fields.
              // let phone = [node.reg_mobiil, node.reg_telefon]
              //   .filter(function(item) {
              //     return item != null
              //   })
              //   .join(' ')

              let maakond = node.aadr_maakond_tekst.replace(' MAAKOND', '')

              let tableRowsComponent
              tableRowsComponent = (
                <tr key={id}>
                  <td>
                    <Link to={path}>
                      {node.asuk_registri_nr} {node.aadr_aadress}
                    </Link>
                  </td>
                  <td>{node.vastut_nimi}</td>
                  {/* <td>{phone}</td> */}

                  <td>{maakond}</td>
                  <td>{node.reg_kp}</td>
                </tr>
              )
              return tableRowsComponent
            })}
          </tbody>
        </table>
      </div>
    </Layout>
  )
}

export const query = graphql`
  query {
    allApiariesCsv(
      sort: { fields: [reg_kp], order: DESC }
      filter: { staatus_tekst: { eq: "Kehtiv" } }
      limit: 100
    ) {
      totalCount
      edges {
        node {
          id
          asuk_registri_nr
          aadr_aadress
          aadr_kyla_alev
          aadr_valdlinn_tekst
          aadr_maakond_tekst
          vastut_nimi
          reg_kp(formatString: "DD. MMMM YYYY", locale: "et-EE")
          reg_mobiil
          reg_telefon
          geometry
          staatus_tekst
        }
      }
    }
  }
`
