import React from 'react'
import { graphql } from 'gatsby'
import Layout from '../components/layout'
import SEO from '../components/seo'
import { Map, CircleMarker, LayersControl } from 'react-leaflet'
import WMTSTileLayer from 'react-leaflet-wmts'
import NoSSR from 'react-no-ssr'
import parse from 'wellknown'
import './mesila.css'

// See: https://medium.com/@kyleshevlin/how-to-use-client-side-libraries-in-a-universal-javascript-app-aa552b715f06
const isBrowser = typeof window !== 'undefined'
const { BaseLayer, Overlay } = isBrowser ? LayersControl : () => {}

const Loading = () => <div>Loading...</div>

export default ({ data }) => {
  const apiary = data.apiariesCsv

  // Join address fields.
  let address = [
    apiary.aadr_aadress,
    apiary.aadr_kyla_alev,
    apiary.aadr_valdlinn_tekst,
    apiary.aadr_maakond_tekst,
  ]
    .filter(function(item) {
      return item != null
    })
    .join(', ')

  // Join phone fields.
  let phone = [apiary.reg_mobiil, apiary.reg_telefon]
    .filter(function(item) {
      return item != null
    })
    .join(' ')

  // Convert WKT points to JSON lat, lng using wellknown library.
  let geometry = parse(apiary.geometry)
  let lat = geometry['coordinates'][0]
  let lng = geometry['coordinates'][1]

  if (typeof window !== 'undefined') {
    return (
      <Layout>
        <SEO
          title={apiary.pohi_nr + ' ' + apiary.aadr_aadress + ' mesila'}
          lang="et"
          keywords={[apiary.vastut_nimi, apiary.pohi_nr, `mesi`, `honey`]}
          description={address}
        />
        <div>
          <h3>
            {apiary.pohi_nr} {apiary.aadr_aadress} mesila
          </h3>
          <p>
            <strong>Mesinik: </strong>
            {apiary.vastut_nimi}
            <br />
            <strong>Telefon: </strong>
            {phone}
            <br />
            <strong>Liik: </strong>
            {apiary.liik_tekst}
            <br />
            <strong>PRIA's alates: </strong>
            {apiary.reg_kp}
            <br />
            <strong>Aadress: </strong>
            {address}
          </p>
        </div>
        <NoSSR onSSR={<Loading />}>
          <Map
            key={Math.random()}
            center={[lng, lat]}
            zoom={15}
            minZoom={7}
            maxZoom={18}
          >
            {/* <TileLayer
                attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              />  */}
            <LayersControl position="topright">
              <BaseLayer checked name="Kaart">
                <WMTSTileLayer
                  attribution="Kaart: Maa-amet | Andmed: PRIA"
                  url="https://tiles.maaamet.ee/tm/wmts"
                  layer="kaart"
                  tilematrixSet="GMC"
                  format="image/png"
                  transparent={true}
                  opacity={1}
                />
              </BaseLayer>
              <BaseLayer name="Foto">
                <WMTSTileLayer
                  attribution="Kaart: Maa-amet | Andmed: PRIA"
                  url="https://tiles.maaamet.ee/tm/wmts"
                  layer="foto"
                  tilematrixSet="GMC"
                  format="image/png"
                  transparent={true}
                  opacity={1}
                />
              </BaseLayer>
              <Overlay name="Hübriid">
                <WMTSTileLayer
                  attribution="Kaart: Maa-amet | Andmed: PRIA"
                  url="https://tiles.maaamet.ee/tm/wmts"
                  layer="hybriid"
                  tilematrixSet="GMC"
                  format="image/png"
                  transparent={true}
                  opacity={1}
                />
              </Overlay>
              <Overlay checked name="Mesilad">
                <CircleMarker
                  key={apiary.id}
                  center={[lng, lat]}
                  fillOpacity={0.7}
                  fillColor={`#ffcc00`}
                  color={`#FFFFFF`}
                  weight={1}
                  opacity={1}
                  radius={16}
                ></CircleMarker>
              </Overlay>
            </LayersControl>
          </Map>
        </NoSSR>
      </Layout>
    )
  }
  return null
}

export const query = graphql`
  query($slug: String!) {
    apiariesCsv(pohi_nr: { eq: $slug }) {
      id
      pohi_nr
      aadr_aadress
      aadr_kyla_alev
      aadr_valdlinn_tekst
      aadr_maakond_tekst
      vastut_nimi
      reg_kp(formatString: "DD. MMMM YYYY", locale: "et-EE")
      reg_mobiil
      reg_telefon
      liik_tekst
      geometry
    }
  }
`
